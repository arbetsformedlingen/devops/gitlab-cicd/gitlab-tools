#!/bin/bash
###############################################################################
#
# This file is supposed to be evaluated with CWD in the git repo to be
# in focus.
#
#
###############################################################################
set -euo pipefail
IFS=$'\n\t'



function die {
    echo "error: $1" >&2; exit 1;
}



function gen_changelogdata() {
    local tokenname="${1:-}"
    local proj_id="${2:-}"
    local tag="$(get_latest_changelogged_version)"
    local latest_version="$(get_latest_tag)"

    test -n "$tokenname"   || die "tokenname not defined"
    test -n "$tag"     || die "tag not defined"
    test -n "$proj_id" || die "proj_id not defined"

    if [ "$latest_version" != "$tag" ]; then
	add_changelogdata_from_commits "$tokenname" "$latest_version" "$proj_id"
    fi

}
export -f gen_changelogdata



function add_changelogdata_from_commits() {
    local tokenname="${1:-}"
    local tag="${2:-}"
    local proj_id="${3:-}"

    test -n "$tokenname"   || die "tokenname not defined"
    test -n "$tag"     || die "tag not defined"
    test -n "$proj_id" || die "proj_id not defined"

    (
	REQUIRE="$tokenname" . bitwarden-vault || exit 1
	curl --request POST --header "PRIVATE-TOKEN: ${!tokenname}" --data "version=$tag" "https://gitlab.com/api/v4/projects/$proj_id/repository/changelog"
    )

}
export -f add_changelogdata_from_commits



function get_repo_remote_label() {
    git remote -v | grep '(push)' | perl -pe 's|^(\S+)\s+\S+\s+\S+$|$1|'
}
export -f get_repo_remote_label



function get_repo_host() {
    local host=$(git remote -v | grep '(push)' | perl -pe 's|^\S+\s+(\S+)\s+\S+$|$1|' | perl -pe 's|^([^:/]+).*$|$1|')

    #FIXME: figure out how to resolve ssh Host to Hostname
    host=${host%"-priv"} # urgh, until then, just hardcode my stupid resolve here

    echo "$host"
}
export -f get_repo_host



function get_proj_id() {
    local host=$(get_repo_host)
    local path=$(git remote -v | grep '(push)' | perl -pe 's|^\S+\s+(\S+)\s+\S+$|$1|' | perl -pe 's|^[^:/]+.||' | perl -pe 's|\.git$||; chomp' | perl -pe "s/([^^A-Za-z0-9\-_.!~*'()])/ sprintf '%%%02x', ord \$1 /eg")

    curl -s -H "Content-Type: application/json" "https://$host/api/v4/projects/$path" | jq -r .id
}
export -f get_proj_id



function get_latest_tag() {
    git fetch --tags
    local latest_tag=$(git tag -l --sort=v:refname | grep -P '^v\d+.\d+.\d+$' | sort --version-sort | tail -n1 | sed 's|^v||i')
    if [ -z "$latest_tag" ]; then latest_tag="0.0.0"; fi
    echo "$latest_tag"
}
export -f get_latest_tag



function get_latest_changelogged_version() {
    local version=""

    if [ -f CHANGELOG.md ]; then
	version=$(grep -P '^## \d+.\d+.\d+ \(\d+-\d+-\d+\)$' CHANGELOG.md | head -n 1 | awk '{ print $2 }')
    fi

    echo "$version"
}



function bump_patch() {
    local tag=$(get_latest_tag)
    local maj=$(echo "$tag" | perl -pe 's|^?(\d+)\.(\d+)\.(\d+)$|$1|')
    local min=$(echo "$tag" | perl -pe 's|^?(\d+)\.(\d+)\.(\d+)$|$2|')
    local patch=$(echo "$tag" | perl -pe 's|^?(\d+)\.(\d+)\.(\d+)$|$3|')

    echo "$maj.$min."$[ $patch + 1 ]
}
export -f bump_patch



function bump_min() {
    local tag=$(get_latest_tag)
    local maj=$(echo "$tag" | perl -pe 's|^(\d+)\.(\d+)\.(\d+)$|$1|')
    local min=$(echo "$tag" | perl -pe 's|^(\d+)\.(\d+)\.(\d+)$|$2|')

    echo "$maj.$[ $min + 1 ].0"
}
export -f bump_min



function bump_maj() {
    local tag=$(get_latest_tag)
    local maj=$(echo "$tag" | perl -pe 's|^(\d+)\.(\d+)\.(\d+)$|$1|')

    echo "$[ $maj + 1 ].0.0"
}
export -f bump_maj
