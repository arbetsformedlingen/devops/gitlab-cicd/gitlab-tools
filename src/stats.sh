#!/bin/bash

report=$(mktemp)
trap "rm -f $report" EXIT

cat - > "$report"


FILTER_PUBLIC='select(.visibility == "public")'
FILTER_PRIVATE='select(.visibility != "public")'
FILTER_HAS_CODEOWNERS='select(.has_codeowners == 1)'
FILTER_HAS_LICENSE='select(.license)'
FILTER_HAS_NO_LICENSE='select(.license | . == null or . == "" or . == {})'
FILTER_HAS_README='select(.readme_url)'
FILTER_IS_ARCHIVED='select(.archived == 1)'
FILTER_IS_ARCHIVED_NO_LICENSE="$FILTER_IS_ARCHIVED | $FILTER_HAS_NO_LICENSE"


function report() {
    local title="${1:-not-set}"; shift
    local NR_PROJS="${1:-0}"; shift
    local filter=$(IFS='|' ; echo "$*")

    local NR_MATCHES=$(jq -rc "$filter"' | .path_with_namespace' < "$report" | sort | uniq | wc -l)

    printf "%-43s %d/%d\n" "$title:" "$NR_MATCHES" "$NR_PROJS"
}


NR_PROJS=$(jq -c '.path_with_namespace' < "$report" | sort | uniq | wc -l)
NR_PROJS_PRIV=$(jq -c "$FILTER_PRIVATE | .path_with_namespace" < "$report" | sort | uniq | wc -l)
NR_PROJS_PUBL=$(jq -c "$FILTER_PUBLIC | .path_with_namespace" < "$report"  | sort | uniq | wc -l)


report "Public projects with codeowners"  "$NR_PROJS_PUBL" "$FILTER_PUBLIC" "$FILTER_HAS_CODEOWNERS"
report "Public projects with license"     "$NR_PROJS_PUBL" "$FILTER_PUBLIC" "$FILTER_HAS_LICENSE"
report "Public projects with readme"      "$NR_PROJS_PUBL" "$FILTER_PUBLIC" "$FILTER_HAS_README"
report "Public projects, archived"        "$NR_PROJS_PUBL" "$FILTER_PUBLIC" "$FILTER_IS_ARCHIVED"
report "Public projects without license, archived"  "$NR_PROJS_PUBL" "$FILTER_PUBLIC" "$FILTER_IS_ARCHIVED_NO_LICENSE"



report "Private projects with codeowners" "$NR_PROJS_PRIV" "$FILTER_PRIVATE" "$FILTER_HAS_CODEOWNERS"
report "Private projects with license"    "$NR_PROJS_PRIV" "$FILTER_PRIVATE" "$FILTER_HAS_LICENSE"
report "Private projects with readme"     "$NR_PROJS_PRIV" "$FILTER_PRIVATE" "$FILTER_HAS_README"
report "Private projects, archived"       "$NR_PROJS_PRIV" "$FILTER_PRIVATE" "$FILTER_IS_ARCHIVED"
report "Private projects without license, archived"  "$NR_PROJS_PUBL" "$FILTER_PRIVATE" "$FILTER_IS_ARCHIVED_NO_LICENSE"
