# NAME

list-gitlab-stuff - a script to dump data from the Gitlab API

## SYNOPSIS

list-gitlab-stuff \[options\]

    Options:
     -g, --groups             List groups instead of projects
     -s, --secrets FILE       Path to secrets file (optional)
     -i, --id GROUP_ID        Top Gitlab group ID (default: 7094694)
     -h, --help               Show this help message

    Environment variables:
     GL_DOMAIN                Gitlab domain (default: https://gitlab.com)
     GL_TOKEN                 Gitlab API token (required if no secrets file)

## RUNNING FROM PODMAN OR DOCKER

```
  podman build -t list-gitlab-stuff .
  podman run --rm -i -e GL\_TOKEN="your-token" list-gitlab-stuff > result.jsonl
```

## DESCRIPTION

This program recursively extracts data from a top group in
Gitlab. Most data is readily available with API queries, but some
extra fields are added to these responses.

## Results

The output is in JSONL format.

## INSTALLATION

Either build a podman image as in the example above or setup your environment with
the requirements demonstrated in Dockerfile.

## CONFIGURATION

You can configure the script either through a secrets file or environment variables.

Using a secrets file with two fields:

    GL_DOMAIN="https://gitlab.com"
    GL_TOKEN="<your token>"

Or using environment variables:

    export GL_DOMAIN="https://gitlab.com"  # Optional, defaults to https://gitlab.com
    export GL_TOKEN="<your token>"         # Required if no secrets file provided

Use a Gitlab token with sufficiently high API permission, and make sure you keep it protected.
If using a secrets file, set `chmod go= secrets.sh` to prohibit other users from reading it.

## UTILITIES

The main program produces a JSONL-file which can be fed to the utility script `stats.sh`:
```
$ ./list-gitlab-stuff.pl secrets.sh > results.jsonl
$ bash stats.sh < results.jsonl

Projects with Aardvark:                  130/436
Projects with Gitlab CI:                 57/436
Projects with codeowners:                8/436
Projects with license:                   127/436
Projects with JobTech CI:                36/436
Projects with readme:                    354/436
Public projects with Aardvark:           120/436
Public projects with Gitlab CI:          51/436
Public projects with codeowners:         7/436
Public projects with license:            119/436
Public projects with JobTech CI:         32/436
Public projects with readme:             308/436
Private projects with Aardvark:          10/436
Private projects with Gitlab CI:         6/436
Private projects with codeowners:        1/436
Private projects with license:           8/436
Private projects with JobTech CI:        4/436
Private projects with readme:            46/436
```

## AUTHOR

Written by Per Weijnitz.

## CONTRIBUTION AND REPORTING BUGS

Yes please - check the Project's Gitlab page.

## LICENSE
GPLv3
