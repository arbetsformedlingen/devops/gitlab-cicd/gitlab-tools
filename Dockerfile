FROM docker.io/library/alpine:3.21.3 AS base

RUN apk add --no-cache perl perl-json perl-libwww perl-lwp-protocol-https jq \
    perl-ipc-system-simple

WORKDIR /app

COPY src/list-gitlab-stuff.pl src/stats.sh /app/



###############################################################################
FROM base AS test

RUN ./list-gitlab-stuff.pl -h >/dev/null &&\
    touch /.tests-successful



###############################################################################
FROM base AS final

COPY --from=test /.tests-successful /

RUN rm -rf /var/cache/apk/*

ENTRYPOINT [ "/app/list-gitlab-stuff.pl" ]
